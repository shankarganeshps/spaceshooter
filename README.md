# Space Shooter

This is a Git + Git LFS repository containing Unity Technologies' [Space Shooter Tutorial](https://unity3d.com/learn/tutorials/projects/space-shooter-tutorial). 

The source has been modified to demonstrate Bitbucket's Unity Cloud Build integration.
